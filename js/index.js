function validateName() {
    var nameInput = document.getElementById("name");
    var errorMessage = document.getElementById("name-error");

    if (nameInput.value.trim() === "") {
      errorMessage.innerHTML = "the field is required.";
    } else {
      errorMessage.innerHTML = "";
    }
  }

  function validateEmail() {
    var emailInput = document.getElementById("email");
    var errorMessage = document.getElementById("email-error");

    if (emailInput.value.trim() === "") {
      errorMessage.innerHTML = "invalid email addres.";
    } else {
      errorMessage.innerHTML = "";
    }
  }

  function validateMessage() {
    var messageInput = document.getElementById("message");
    var errorMessage = document.getElementById("message-error");

    if (messageInput.value.trim() === "") {
      errorMessage.innerHTML = "the field is required.";
    } else {
      errorMessage.innerHTML = "";
    }
  }

  function validateForm() {
    validateName();
    validateEmail();
    validateMessage();

    var nameInput = document.getElementById("name");
    var emailInput = document.getElementById("email");
    var messageInput = document.getElementById("message");

    if (
      nameInput.value.trim() === "" ||
      emailInput.value.trim() === "" ||
      messageInput.value.trim() === ""
    ) {
      return false;
    }
  }